#include "template.hpp"

int main() {
  string str;
  cin >> str;
  reverse(str.begin(), str.end());
  cout << str << endl;
}
