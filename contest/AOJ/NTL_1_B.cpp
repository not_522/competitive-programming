#include "math/mint.hpp"
#include "math/pow.hpp"

int main() {
  Mint m;
  int n;
  cin >> m >> n;
  cout << pow(m, n) << endl;
}
