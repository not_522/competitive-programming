#include "template.hpp"

int main() {
  int64_t n;
  cin >> n;
  cout << (n % 2 ? n : n / 2) << endl;
}
