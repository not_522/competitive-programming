#include "template.hpp"

int main() {
  int64_t n;
  cin >> n;
  cout << (n / 2 + 1) * ((n + 1) / 2 + 1) - 1 << endl;
}
