#include "template.hpp"

int main() {
  int64_t a, b;
  cin >> a >> b;
  cout << (a >= b ? "S" : "K") << endl;
}
