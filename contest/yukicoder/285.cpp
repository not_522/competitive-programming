#include "template.hpp"

int main() {
  long double d;
  cin >> d;
  cout << setprecision(2) << d * 1.08 << endl;
}
