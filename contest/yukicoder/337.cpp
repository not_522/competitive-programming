#include "template.hpp"

int main() {
  int n, p;
  cin >> n >> p;
  cout << (p == n * p ? "=" : "!=") << endl;
}
