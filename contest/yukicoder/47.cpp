#include "bit_operation.hpp"

int main() {
  int n;
  cin >> n;
  cout << most_bit(n - 1) + 1 << endl;
}
