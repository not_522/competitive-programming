#include "template.hpp"

int main() {
  int x, y;
  cin >> x >> y;
  cout << (x < y ? "Better" : "Worse") << endl;
}
