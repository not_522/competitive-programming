#include "template.hpp"

int main() {
  char a, b;
  cin >> a >> b;
  cout << (a == b ? 'H' : 'D') << endl;
}
