#include "template.hpp"

int main() {
  int m, d;
  cin >> m >> d;
  cout << (m % d ? "NO" : "YES") << endl;
}
