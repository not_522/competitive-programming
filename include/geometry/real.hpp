#pragma once
#include "arithmetic.hpp"
#include "ordered.hpp"
#include "math/basic.hpp"

class Real : public Arithmetic<Real>, public Ordered<Real> {
private:
  long double val;

public:
  static long double EPS;

  Real() {}

  Real(long double val) : val(val) {}

  Real operator-() const {return -val;}

  template<typename T> Real operator+=(const T& r) {
    val += static_cast<long double>(r);
    return *this;
  }

  template<typename T> Real operator-=(const T& r) {
    val -= static_cast<long double>(r);
    return *this;
  }

  template<typename T> Real operator*=(const T& r) {
    val *= static_cast<long double>(r);
    return *this;
  }

  template<typename T> Real operator/=(const T& r) {
    val /= static_cast<long double>(r);
    return *this;
  }

  template<typename T> Real operator-(const T& v) const {return Real(*this) -= v;}

  template<typename T> Real operator*(const T& v) const {return Real(*this) *= v;}

  template<typename T> Real operator/(const T& v) const {return Real(*this) /= v;}

  template<typename T> bool operator<(const T r) const {return val < static_cast<long double>(r) - EPS;}

  Real abs() const {return std::abs(val);}

  Real sqrt() const {return std::sqrt(val);}

  operator long double() const {return val;}
};

long double Real::EPS = 1e-10;

ostream& operator<<(ostream& os, const Real& a) {
  os << a;
  return os;
}

istream& operator>>(istream& is, Real& a) {
  long double n;
  is >> n;
  a = n;
  return is;
}

Real floor(const Real& r) {
  return floor(r);
}
